import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  data:any;
  ans:string;
  answer:string;
  num:any;
  id:number;
  add:any;
  i:number=0;

  questionArray:Array<any> = [
    {
      id:0,
      answer:"Kathmandu",
      options:["Kathmandu","Lalitpur","Bhaktapur"],
      question:"What is the capital of Nepal?"
    },
    {
      id:1,
      answer:"Mount Everest",
      options:["Mount Everest","Annapurna Sanctuary","Manaslu","Kanchenjunga","Lhotse"],
      question:"Which is the world's highest mountain? "
    },
    {
      id:2,
      answer:"KP Sharma Oli",
      options:["Bidya Devi Bhandari","Pushpa Kamal Dahal","KP Sharma Oli"],
      question:"Who is the prime minister of Nepal? "
    },
    {
      id:3,
      answer:"True",
      options:["True","False"],
      question:"Is Kathmandu capital city of Nepal? "
    }

];
  constructor(
    private router: Router,
    private routes: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.routes.queryParams.subscribe((params)=>{
      this.data=params;
      //console.log(this.data);
      if(this.data.question != null){
        this.id=this.questionArray.length;
        this.add= Object.assign(this.id,this.data);
        console.log(this.add);
        //console.log(this.data);
        this.questionArray.push(this.add);
        
      }
      //console.log(this.data);
      //console.log(this.questionArray);
      //console.log(this.questionArray.length);
      
    })
    
  }

  onSubmit(anss:any,num:number){
      this.answer=anss.ans;
      this.num=num;
      console.log(this.questionArray[num].answer);
      console.log(this.answer);
      if( this.questionArray[num].answer == this.answer){
        alert("Correct Answer")
      }
      else{
        alert("Wrong Answer")
      }
      

  }
}
