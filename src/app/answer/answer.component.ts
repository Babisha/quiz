import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

  data:any;
  ans:string;
  answer:string;
  constructor(
    private router: Router,
    private routes: ActivatedRoute
  ) { }

  ngOnInit(): void {

  this.routes.queryParams.subscribe((params)=>{
    //console.log(params);
    this.data=params;
  })
  }

  onSubmit(anss:any){
    alert('Question added succesfully.')
    this.answer = anss.ans;
    /*console.log(this.answer);
    console.log(this.data.question);
    console.log(this.data.options);
    console.log(this.data.options.length);*/
    this.router.navigate(['/quiz'],{
      queryParams:{question:this.data.question,options:this.data.options,answer:this.answer}
    });
    
  }

}


