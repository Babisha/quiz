import { Routes } from '@angular/router';
import { QuizComponent } from './quiz/quiz.component';
import { QuestionComponent } from './question/question.component';
import { AnswerComponent } from './answer/answer.component';
import { HomeComponent } from './home/home.component';

export const appRoutes : Routes = [
    {path:"quiz",component:QuizComponent},
    {path:"question",component:QuestionComponent},
    {path:"answer",component:AnswerComponent},
    {path:"",component:HomeComponent}
]