import { Component, OnInit } from '@angular/core';
import {  Validators, FormBuilder, FormArray, FormControl, ControlContainer} from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute
    ) { }

    userForm = this.fb.group({
    question: ['',Validators.required],

    options: this.fb.array(
    [
      this.fb.control('',Validators.required),
      this.fb.control('',Validators.required)
    ])
  });

  get optionArray(){
    return this.userForm.get("options") as FormArray;
  }

  addOptions(){
    this.optionArray.push(this.fb.control('',Validators.required));
  }

  deleteOptions(index :number){
    this.optionArray.removeAt(index);
  }

  chooseAnswer(){
    let data:any = this.userForm.value;
    //alert(this.optionArray.controls.length);
    //console.log(this.userForm.value.question);
    //console.log(this.userForm.value.options);
    this.router.navigate(['/answer'],{
      queryParams:{question:this.userForm.value.question,options:this.userForm.value.options,length:this.userForm.value.options.length}
    });
    
  }

  ngOnInit(): void {
  }

}
