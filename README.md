# Quiz

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

There are two buttons in home page.

One is to add questions and another is to start quiz.

When you click in add question you will be redirect to page where you can see form where you will be able to add questions and options.

Then when you click to add option button you will be able to add extra options.

And when to click to choose answer button you will be able to choose correct answer for your question.

After you click in submit button your question is added in an question array and you will be redirected to start quiz page where you will be able to play quiz.

The added question will occur as an last question in quiz.

As the questions are stored in array you won't be able to see your added question after refreshing the page.
